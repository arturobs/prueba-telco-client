import React, { Component} from "react";
import jwt_decode from "jwt-decode";

import LogIn from './components/LogIn';
import Register from './components/Register';
import Crud from './components/Crud';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'reactstrap'
import './css/App.css';



class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            logged: false,
            registro: false,
            token: '',
            usuario: '',
            password: '',
            tipo: ''
        };
        
    }

   

    HandleUsuario(e){
        this.setState({
            usuario: e.target.value
        })
    }

    HandlePassword(e){
        this.setState({
            password: e.target.value
        })
    }

    handleTipo(e){
        this.setState({
            tipo: e.target.value
        })
    }

    HandleRegistro(e){
        this.setState({
            registro: true
        })
    }

    HandleSubmit(e){   
        //localStorage.clear();             
          
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },            
            body: JSON.stringify({ usuario: `${this.state.usuario}`, password: `${this.state.password}`})
        };
        

        fetch (`http://localhost:3001/telco/iniciar_sesion`, requestOptions)
        .then((response) => {
          return response.json()
         })
        .then((data) => {          
          const decoded = jwt_decode(data.token);
          localStorage.setItem('userId', decoded.sub);
          localStorage.setItem('tipoUsuario', decoded.tipo);
          localStorage.setItem('token', data.token)
          this.setState({ logged: data.status, password: '', token: data.token })
        })
        e.preventDefault();

    }

    HandleRegisterSubmit(e){
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },            
            body: JSON.stringify({ usuario: `${this.state.usuario}`, password: `${this.state.password}`, tipo: `${this.state.tipo}`})
        };

        fetch (`http://localhost:3001/telco/nuevo_Usuario`, requestOptions)
        .then((response) => {
          return response.json()
         })
        .then((data) => {       
          alert(data.message)
          this.setState({
            usuario: '',
            password: '',
            tipo: '',
            registro: false
        })
          console.log(data);
        })
        e.preventDefault();
    }




    renderApp(){
    if(!this.state.logged){
        if(!this.state.registro){
            return (                
                <LogIn 
                    usuario={this.state.usuario}
                    password={this.state.password}
                    onHandleUsuario={this.HandleUsuario.bind(this)}
                    onHandlePassword={this.HandlePassword.bind(this)}
                    onHandleSubmit={this.HandleSubmit.bind(this)}
                    onHandleRegistro={this.HandleRegistro.bind(this)}
                />
            )
        }else{
            return(
                <Register 
                    usuario={this.state.usuario}
                    password={this.state.password}
                    onHandleUsuario={this.HandleUsuario.bind(this)}
                    onHandlePassword={this.HandlePassword.bind(this)}
                    onHandleTipo={this.handleTipo.bind(this)}
                    onHandleRegisterSubmit={this.HandleRegisterSubmit.bind(this)}
                />
            )            
        }
        
        }else{
            return (
                <Crud />
            )
    }
}

    render(){
        return(
            <Container>
                <div className="App">
                    <header className="App-header">
                        <h1 className="App-title">Bienvenido al TelcoSpa</h1>
                        </header>
                    <div className="App-intro">
                        <br/>
                        <br/>
                        {this.renderApp()}
                    </div>
                </div>
            </Container>
            

        )
    };
}






export default App;
