import React, { Component } from 'react'
import {Button, Input, Container } from 'reactstrap'
import '../css/Register.css';

class Register extends Component {
  render(){
    return(
      <>
        <Container className='Register'>
          <div className='App-form'>
            <div className='text-center'>
              <h3>Registro</h3>
            </div>
            <form onSubmit={this.props.onHandleRegisterSubmit}>
              <br/>
              <Input type='text' className='campo1' placeholder='Usuario' value={this.props.usuario} onChange={this.props.onHandleUsuario}/>
              <br/>            
              <Input type='password' className='campo2' placeholder='Password' value={this.props.password} onChange={this.props.onHandlePassword}/>
              <br/>
              <Input type='select' onChange={this.props.onHandleTipo}>
                <option disabled selected>Seleccione Tipo De Usuario</option>
                <option value="1">Administrador</option>
                <option value="2">Usuario</option>
              </Input>
              <br/>
              <br/>
              <div className='text-center'>
                <Button type='submit' color='success'>Enviar</Button>
              </div>              
            </form>
          </div>
        </Container>
      </>
        
    )
    
  }
}

export default Register;