import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter} from 'reactstrap'
import GoogleMapReact from 'google-map-react';
import Marker from './Marker';


class Crud extends Component {

  constructor(props) { 
    super(props); 
    this.state = { 
      data:[],
      form:{
        Placas:'',
        Marca:'',
        Color:'',
        Modelo:'',
        Posicion:'',
      },
      center: {
        lat: 20.676627192836833,
        lng: -103.41765480153282
      },
      latm: 0,
      lngm: 0,
      zoom: 5,
      modalInsertar: false,
      modalEditar: false
     }; 
} 

getVehiculos=()=>{
  var id = localStorage.getItem('userId');
  var tipo = localStorage.getItem('tipoUsuario');
  var token = localStorage.getItem('token');

  const requestOptions = {
    method: 'GET',
    headers: { 'Authorization': `bearer ${token}` }
};

  fetch (`http://localhost:3001/telco/get_Vehiculos?ui=${id}&ut=${tipo}`, requestOptions)
        .then((response) => {
          return response.json()
         })
        .then((data) => {  
          this.setState({data:data.results})        
          //console.log(this.state.data);
          //console.log(data1);
        })
}

componentDidMount(){
  this.getVehiculos();
}

handleChange= async e=>{
  e.persist();
  await this.setState({
    form:{
      ...this.state.form,
      [e.target.name]: e.target.value
    }
  });
}

mostrarModalInsert=()=>{
  this.setState({modalInsertar: true});
}

mostrarModalEdit=(registro)=>{
  this.setState({modalEditar: true, form: registro});
}

ocultarModalInsert=()=>{
  this.setState({modalInsertar: false});
}

ocultarModalEdit=()=>{
  this.setState({modalEditar: false});
}

insertVehiculo=()=>{
  var dato = {...this.state.form};
  var token = localStorage.getItem('token');
  var id = localStorage.getItem('userId');

  const requestOptions = {
    method: 'POST',
    headers: { 'Authorization': `bearer ${token}`,'Content-Type': 'application/json'},
    body: JSON.stringify({ placas: `${dato.Placas}`, marca: `${dato.Marca}`, color: `${dato.Color}`, modelo: `${dato.Modelo}`, posicion: `${dato.Posicion}`, usuario_id: `${id}`})
};

console.log(requestOptions);

  fetch (`http://localhost:3001/telco/nuevo_Vehiculo`, requestOptions)
        .then((response) => {
          return response.json()
         })
        .then((data) => {  
          this.getVehiculos();
        })
  this.setState({modalInsertar: false});
}

editVehiculo=(dato)=>{

  var token = localStorage.getItem('token');

  const requestOptions = {
    method: 'PUT',
    headers: { 'Authorization': `bearer ${token}`,'Content-Type': 'application/json' },
    body: JSON.stringify({ placas: dato.Placas, marca: dato.Marca, color: dato.Color, modelo: dato.Modelo, posicion: dato.Posicion})
};

  fetch (`http://localhost:3001/telco/update_Vehiculos`, requestOptions)
        .then((response) => {
          return response.json()
         })
        .then((data) => {  
          this.getVehiculos();
        })
  this.setState({modalEditar: false}); 
}

deleteVehiculo=(dato)=>{
  var opcion = window.confirm(`Realmente desea eliminar el vehiculo ${dato.Placas}?`);
  var token = localStorage.getItem('token');

  if(opcion){
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Authorization': `bearer ${token}`,'Content-Type': 'application/json' },
      body: JSON.stringify({ placas: dato.Placas, marca: dato.Marca, color: dato.Color, modelo: dato.Modelo, posicion: dato.Posicion})
  };
  
    fetch (`http://localhost:3001/telco/delete_Vehiculos`, requestOptions)
          .then((response) => {
            return response.json()
           })
          .then((data) => {  
            this.getVehiculos();
          })
  }
  
  
}

rastrearVehiculo=(dato)=>{
  var coord = dato.Posicion.split(',');

  this.setState({
    center: {
      lat: parseFloat(coord[0]),
      lng: parseFloat(coord[1])
    },
    latm: parseFloat(coord[0]),
    lngm: parseFloat(coord[1]),
    zoom: 15
  })
}

  render(){ 
    return(
      <>
        <Container>
          <br/>
          <Button color='success' onClick={()=>this.mostrarModalInsert()}>Insertar Vehiculo</Button>
          <br/><br/>
          <Table>
              <thead>
                <tr>
                  <th>Placas</th>
                  <th>Marca</th>
                  <th>Color</th>
                  <th>Modelo</th>
                  <th>Posicion</th>
                  <th>Acciones</th>
                </tr>
              </thead>

              <tbody>
                {this.state.data.map((element) => (
                  <tr>
                    <td>{element.Placas}</td>
                    <td>{element.Marca}</td>
                    <td>{element.Color}</td>
                    <td>{element.Modelo}</td>
                    <td>{element.Posicion}</td>
                    <td><Button color='warning' onClick={()=>this.mostrarModalEdit(element)}>Editar</Button>{"  "}
                    <Button color='danger' onClick={()=>this.deleteVehiculo(element)}>Eliminar</Button>{"  "}
                    <Button color='primary' onClick={()=>this.rastrearVehiculo(element)}>Rastrear</Button></td>
                  </tr>

                ))}
              </tbody>
          </Table>

          

          <div style={{ height: '50vh', width: '100%' }}>
            <GoogleMapReact
              bootstrapURLKeys={{ key: 'AIzaSyDBXITyf1MawFEIk4jRMkiOO67jH50MNhI' }}
              //defaultCenter={this.state.center}
              center={this.state.center}
              zoom={this.state.zoom}
            >
              
            <Marker
                lat={this.state.latm}
                lng={this.state.lngm}
            />

          
            </GoogleMapReact>
          </div>
        </Container>

        <Modal isOpen={this.state.modalInsertar}>

          <ModalHeader>
            <div><h3>Insertar Vehiculo</h3></div>
          </ModalHeader>

          <ModalBody>
            
            <FormGroup>
              <label>Placas:</label>
              <input className='form-control' name='Placas' type='text' onChange={this.handleChange} />              
            </FormGroup>

            <FormGroup>
              <label>Marca:</label>
              <input className='form-control' name='Marca' type='text' onChange={this.handleChange} />              
            </FormGroup>

            <FormGroup>
              <label>Color:</label>
              <input className='form-control' name='Color' type='text' onChange={this.handleChange} />              
            </FormGroup>

            <FormGroup>
              <label>Modelo:</label>
              <input className='form-control' name='Modelo' type='text' onChange={this.handleChange} />              
            </FormGroup>

            <FormGroup>
              <label>Posicion:</label>
              <input className='form-control' name='Posicion' type='text' placeholder='Latitud,Longitud(sin espacios)' onChange={this.handleChange} />              
            </FormGroup>

          </ModalBody>

          <ModalFooter>
              <Button color='success' onClick={()=>this.insertVehiculo()}>Insertar</Button>
              <Button color='danger' onClick={()=>this.ocultarModalInsert()}>Cancelar</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalEditar}>

          <ModalHeader>
            <div><h3>Editar Vehiculo</h3></div>
          </ModalHeader>

          <ModalBody>
            
            <FormGroup>
              <label>Placas:</label>
              <input className='form-control' readOnly name='Placas' type='text' onChange={this.handleChange} value={this.state.form.Placas}/>              
            </FormGroup>

            <FormGroup>
              <label>Marca:</label>
              <input className='form-control' name='Marca' type='text' onChange={this.handleChange} value={this.state.form.Marca}/>              
            </FormGroup>

            <FormGroup>
              <label>Color:</label>
              <input className='form-control' name='Color' type='text' onChange={this.handleChange} value={this.state.form.Color}/>              
            </FormGroup>

            <FormGroup>
              <label>Modelo:</label>
              <input className='form-control' name='Modelo' type='text' onChange={this.handleChange} value={this.state.form.Modelo}/>              
            </FormGroup>

            <FormGroup>
              <label>Posicion:</label>
              <input className='form-control' name='Posicion' type='text' onChange={this.handleChange} value={this.state.form.Posicion}/>              
            </FormGroup>

          </ModalBody>

          <ModalFooter>
              <Button color='success' onClick={()=>this.editVehiculo(this.state.form)}>Editar</Button>
              <Button color='danger' onClick={()=>this.ocultarModalEdit()}>Cancelar</Button>
          </ModalFooter>
        </Modal>
      </>
      
    )
    
  }
}

export default Crud;
