import React from 'react'

export function Prueba({ pruebas }) {
  return (
    <>
        <h1>Prueba</h1>
        <ul>

            {pruebas.map( prueba => (
                
                <li key={prueba["Placas"]}>
                    {prueba.Placas} {prueba.Marca} {prueba.Modelo} {prueba.Posicion}                                                            
                </li>
            ))}
        </ul>
        </>
  )
}
