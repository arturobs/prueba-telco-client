import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button, Input, Container } from 'reactstrap'
import '../css/LogIn.css';

class LogIn extends Component {
  render(){
    return(
      <>
        <Container className='LogIn'>
          <div className='App-form'>
          <form onSubmit={this.props.onHandleSubmit}>
            <br/>
            <Input type='text' className='campo1' placeholder='Usuario' value={this.props.usuario} onChange={this.props.onHandleUsuario}/>
            <br/>       
            <br/>
            <Input type='password' className='campo2' placeholder='Password' value={this.props.password} onChange={this.props.onHandlePassword}/>
            <br/><br/>
            <div className='text-center'>
              <div className='btn-group'>
                <Button color='success' type='submit'>Enviar</Button>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Button color='primary' type='button' onClick={this.props.onHandleRegistro}>Registrarse</Button>
              </div>
            </div>
                        
          </form>
        </div>
        </Container>
      </>
      
    )
    
  }
}

export default LogIn;